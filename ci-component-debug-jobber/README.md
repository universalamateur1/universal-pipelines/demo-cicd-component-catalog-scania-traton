# CI-Component - Debug-Jobber

## Usage

You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: gitlab.com/universalamateur1/universal-pipelines/ci-component-debug-jobber/debugg-jobber@<specific-version>
  - component: <fully-qualified-domain-name>/<project-path>/<component-name>@<specific-version>
```

where `<specific-version>` in order of highest priority first, can be:

- A commit SHA, for example `e3262fdd0914fa823210cdb79a8c421e2cef79d8`.
- A tag, for example: `1.0.0`. If a tag and commit SHA exist with the same name, the commit SHA takes precedence over the tag.
- A branch name, for example `main`. If a branch and tag exist with the same name, the tag takes precedence over the branch.
- `~latest`, which is a special version that always points to the most recent release published in the CI/CD Catalog.

You can adjust the stage by giving it as input

```yaml
include:
  # 1: include the component
  - component: gitlab.com/universalamateur1/universal-pipelines/ci-component-debug-jobber/debugg-jobber@1.0.1
    # 2: set/override component inputs
    inputs:
      stage: build
  # 3; Include the same component with a diferent inout
  - component: gitlab.com/universalamateur1/universal-pipelines/ci-component-debug-jobber/debugg-jobber@~latest
    # 4: setanother stage and with it another job name
    inputs:
      stage: '.post'
```

### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `test`      | The stage where you want this job to be added. The component can be added multiple times with different stage inputas and runs then multiple times in one pipeline with seperat job names. |

## Documentaiton

- Please read about CI/CD components and best practices at [CI/CD components](https://docs.gitlab.com/ee/ci/components/)
- Find more in [CI/CD component examples](https://docs.gitlab.com/ee/ci/components/examples.html)
